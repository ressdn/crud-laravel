<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CrudController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CrudController::class, 'index']);

Route::get('/tambah', [CrudController::class, 'create']);
route::post('/proses-tambah', [CrudController::class, 'store']);

Route::get('/edit-data/{id}/edit', [CrudController::class, 'edit']);
route::post('/proses-edit/{id}', [CrudController::class, 'update']);
route::delete('/delete/{id}', [CrudController::class, 'destroy']);