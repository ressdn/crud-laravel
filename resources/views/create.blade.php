@extends('layouts.main')

@section('container')

<div class="container-fluid">
                        <h1 class="mt-4">Tambah User</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Tambah User</li>
                        </ol>
                        <form class="user px-5" action="/proses-tambah" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="container">
                                <div class="mb-3">
                                    <label for="nama" class="form-label">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="name">
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="text" class="form-control" id="email" name="email">
                                </div>
                                <div class="mb-3">
                                    <label for="notlp" class="form-label">No. Tlp</label>
                                    <input type="text" class="form-control" id="notlp" name="noTlp">
                                </div>
                                <div class="mb-3">
                                    <label for="alamat" class="form-label">Alamat</label>
                                    <input type="text" class="form-control" id="alamat" name="alamat">
                                </div>
                                    <button type="submit"class="btn btn-primary btn-user btn-block" name="tambah">
                                        Tambah Data
                                    </button>
                        </form>
                    </div>

@endsection